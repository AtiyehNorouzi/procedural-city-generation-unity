﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public enum WallType
    {
        Normal
    }

    public WallType wallType;

    public Wall(WallType type)
    {
        wallType = type;
    }

}
