﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    [SerializeField]
    private Room[,] rooms;

    private int floorNumber;

    public Floor(Room[,] rooms, int floorNumber)
    {
        this.rooms = rooms;
        FloorNumber = floorNumber;
    }

    public Room[,] Rooms
    {
        get => rooms;
        set => rooms = value;
    }

    public int FloorNumber
    {
        get => floorNumber;
        set => floorNumber = value;
    }
}
