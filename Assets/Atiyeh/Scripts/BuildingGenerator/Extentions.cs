﻿
using UnityEngine;

public static class Extentions
{
    public static void ApplyMaterial(this MeshRenderer renderer, string shaderName, string objectName)
    {
        var randomMat = new Material(Shader.Find(shaderName))
        {
            name = $"{objectName}_material",
            color = GetRandomColor()
        };
        renderer.material = randomMat;
    }
    static Color GetRandomColor() => Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f, 0.1f, 1);
}
