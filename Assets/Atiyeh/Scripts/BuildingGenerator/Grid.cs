﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Created by Smokey Shadow
/// This code is responsible for generating cube buildings with given parameters
/// </summary>
public class Grid : MonoBehaviour
{
    [SerializeField, Range(1, 200)]
    private int gridWidth;

    [SerializeField, Range(1, 200)]
    private int gridHeight;

    [SerializeField, Range(1, 200)]
    private int shapeWidth;

    [SerializeField, Range(1, 200)]
    private int shapeHeight;

    [SerializeField, Range(1, 200)]
    private int shapeDepth;

    [SerializeField, Range(1, 200)]
    private int randomMaxHeight;

    [SerializeField]
    private int randomSeed;

    [SerializeField]
    private ShapeType shapeType;

    [SerializeField]
    private string shaderName;

    [SerializeField]
    private Texture buildingTexture;

    GameObject[,] grid;
   
    void Start()
    {
        Random.InitState(randomSeed);
        grid = new GameObject[gridWidth, gridHeight];
    }

    void FillGrid()
    {
        for (int col = 0; col < gridHeight; col++)
        {
            for (int row = 0; row < gridWidth; row++)
            {
                GameObject cell = null;
                if (grid[row, col] == null)
                {
                    cell = new GameObject($"cell_{row}_{col}");
                    cell.transform.parent = gameObject.transform;
                    grid[row, col] = cell;
                }
                else
                {
                    DestroyImmediate(grid[row, col]);
                    grid[row, col] = new GameObject($"cell_{row}_{col}");
                    cell = grid[row, col];
                    cell.transform.parent = gameObject.transform;
                }
                int randHeight = Random.Range(1, randomMaxHeight) * shapeHeight;
                cell.transform.position = new Vector3(row*shapeWidth , 0 , col*shapeDepth);
                var meshFilter = cell.AddComponent<MeshFilter>();
                var meshRenderer = cell.AddComponent<MeshRenderer>();
                meshFilter.mesh = new Cube { Width = shapeWidth, Height = randHeight, Depth = shapeDepth }.Generate();
                meshRenderer.ApplyMaterial(shaderName, cell.name);
                //we can add texture to it in future
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            FillGrid();
        }
    }

   
}
