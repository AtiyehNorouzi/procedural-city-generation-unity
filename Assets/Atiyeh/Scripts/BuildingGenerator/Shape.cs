﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shape
{
    public ShapeType ShapeType { get; set; }

    public int Width { get; set; }

    public int Height {  get; set; }

    public int Depth { get; set; }

    public abstract Mesh Generate();
}
